(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2018
//
// Classroom: CAS 220
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Midterm 2
//
(* ****** ****** *)
//
// Out: 9:00am on the 21st of June, 2018
// Due: 11:59pm on the 23rd of June, 2018
//
(* ****** ****** *)
//
// ATTENTION:
// Absolutely no collaboration in any form is allowed!!!
//
(* ****** ****** *)

(*
//
NOTE:
80 points = 100%
//
*)
(*
//
// First group:
//
// Please select 6
// from this group:
//
Wallis: 10 points
mystream_235: 10 points
pangram_check: 10 points
mystream_group: 10 points
mystream_remdup: 10 points
mytree_avltest: 10 points
mytree_postordize: 10 points
mytree_streamize_bfs: 10 points
//
// Second group:
//
// Please select 2
// from this group:
//
mylist_select: 20 points
Sudoku_search: 20 points
//
//
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
// HX: First group
//
(* ****** ****** *)
(*
**
** HX: 10 points
**
*)
(*
You can find Wallis product here:
http://en.wikipedia.org/wiki/Wallis_product
Please implement a function that generates a stream
consisting of all the partial products in Wallis product:
*)
//
extern
fun Wallis((*void*)): stream(double)
//
(* ****** ****** *)
//
(*
** HX: 10 points
**
** [mystream_235] returns a stream of integers
** enumerated in ascending order such that each
** integer in the stream can be written in the
** following form: (2^p)(3^q)(5^r), where the exponents
** p, q, and r are non-negative integers. For instance,
** the first few integers in this stream are:
** 1(=(2^0)(3^0)(5^0))
** 2(=(2^1)(3^0)(5^0))
** 3(=(2^0)(3^1)(5^0))
** 4(=(2^2)(3^0)(5^0))
** 5(=(2^0)(3^0)(5^1))
** 6(=(2^1)(3^1)(5^0))
** 8(=(2^3)(3^0)(5^0))
** 9(=(2^0)(3^2)(5^0))
** 10(=(2^1)(3^0)(5^1))
** 12(=(2^2)(3^1)(5^0))
** ...
**
*)
//
extern
fun mystream_235(): stream(int)
//
(* ****** ****** *)
//
// HX: 10 points
//
// A string is a pangram if it contains all of
// the letters in the (English) alphabet. Letters
// are case-insensitive here. Given a string, the
// function [pangram_check] returns true if and only
// if the string is a pangram.
//
(*
//
// You may use the following function in the library
// to turn string into a list of characters:
//
// fun string_explode(cs: string): list0(char)
//
*)
extern
fun
pangram_check(text: string): bool
//
(* ****** ****** *)
//
(*
** HX: 10 points
**
** Given an ordered list, [mystream_group]
** returns another stream that consists of
** pairs indicating how many times each element
** in the given stream occurs.
**
** For instance, assume the following stream
** is given:
**
** ("a", "b", "b", "c", "d", "d", "d", ...)
**
** Then the returned stream should be
** ($tup("a", 1), $tup("b", 2), $tup("c", 1), $tup("d", 3), ...)
*)
//
extern
fun
{a:t@ype}
mystream_group
  (xs: stream(a)): stream($tup(a, int))
//
(* ****** ****** *)
//
// HX: 10 points
//
// [mystream_remdup] removes all the duplicates in a
// given stream. The second argument is a function for
// testing whether two elements in the stream are considered
// equal
//
// For instance, assume xs is
// (1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5, ...)
// Then the returned stream should be (1, 2, 3, 4, 5, ...)
// (if [eqfn] is the usual equality on integers)
//
extern
fun
{a:t@ype}
mystream_remdup
  (xs: stream(a), eqfn: (a, a) -<cloref1> bool): stream(a)
//
(* ****** ****** *)
//
datatype
mytree(a:t@ype) =
| mytree_nil of ()
| mytree_cons of (mytree(a), a, mytree(a))
//
(* ****** ****** *)
(*
//
// A tree t0 is an AVL tree if
// (1) t0 is mytree_nil(), or
// (2) t0 is mytree_cons(tl, x, tr) such that
// both tl and tr are AVL trees and the *height*
// difference between tl and tr is at most one.
//
// Please implement the following function in the
// style given in the following file:
// lecture-06-19/mytree_is_braun.dats
// You need to make use of exception to ensure that
// your implementation of mytree_avltree only traverses
// a given tree exactly once to tell where the tree is
// AVL
//
*)
extern
fun
{a:t@ype}
mytree_avltest(t0: mytree(a)): bool
//
(* ****** ****** *)
//
// HX: 10 points
// [mytree_postordize] enumerates a
// given mytree-value in the post-order manner:
// For each node, the elements in its left subtree
// are enumnerated; then the elements in its right
// subtree are enumerated; then the element stored
// in the node is enumerated.
//
extern
fun
{a:t@ype}
mytree_postordize(t0: mytree(a)): stream(a)
//
(* ****** ****** *)

(*
//
// HX:
// please implement
// mytree_streamize_bfs that turns a
// given tree into a stream which enumerates
// the elements of the tree in a breadth-first
// fashion. For instance the following tree is
// enumerated as 0, 1, 2, 3, 4, 5, 6:
//
                  0
               1     2
              3 4     5
                 6   
*)
//
extern
fun
{a:t@ype}
mytree_streamize_bfs(t0: mytree(a)): stream(a)

(* ****** ****** *)
//
// HX: Second group
//
(* ****** ****** *)
//
// G2-1: 20 points
//
// Given a list xs of length m and an
// integer n <= m, please find a stream
// of all the possible pairs (xs1, xs2)
// such that xs1 and xs2 are formed by taking
// n and m-n elements from xs, respectively:
//   length(xs1) = n and length(xs2) = m-n
// and the union of xs1 and xs2 gives back xs
//
// Assume xs = (a,b,c,d) and n = 2. The returned
// stream should consist of the following 6 pairs:
// $tup((a, b), (c, d))
// $tup((a, c), (b, d))
// $tup((a, d), (b, c))
// $tup((b, c), (a, d))
// $tup((b, d), (a, c))
// $tup((c, d), (a, b))
//
extern
fun
{a:t@ype}
mylist_select
(
xs: list0(a), n: int
) : stream($tup(list0(a), list0(a)))
//
(* ****** ****** *)
//
// G2-2: 20 points
//
// Solving Sudoku based on depth-first search
//
// Sudoku is a popular game:
// http://www.7sudoku.com/view-puzzle?date=20171124
//
// A Sudoku board is a 3-by-3 matrix of 3-by-3
// matrices of integer digits. Note that an entry
// is considered blank if the digit 0 is stored
// in it.
//
// A partial board is one that may contain digit 0.
// For instance, the following board is partial:
//
(*
-----------------
     |     |  7 5 
  4  |9 8  |      
  8  |     |      
-----------------
3   2|  1  |5     
9 1  |7    |      
     |    2|    6 
-----------------
     |  6 4|    1 
     |  2  |6     
4    |    5|    2 
-----------------
*)
//
// A full board is one that contains only non-zero
// digits. For instance, the following board is full:
//
(*
-----------------
1 2 9|6 4 3|8 7 5 
5 4 7|9 8 1|2 6 3 
6 8 3|2 5 7|1 4 9 
-----------------
3 6 2|4 1 8|5 9 7 
9 1 5|7 3 6|4 2 8 
8 7 4|5 9 2|3 1 6 
-----------------
2 9 8|3 6 4|7 5 1 
7 5 1|8 2 9|6 3 4 
4 3 6|1 7 5|9 8 2 
-----------------
*)
//
// A board extends another one if
// the former can be obtained by changing
// some of the zero entries in the latter
// into non-zero entries.
//
// The function Sudoku_search takes
// a partial board and returns a stream of
// *all* full boards that extend the partial
// board.
//
typedef
board =
matrix0(matrix0(int))
//
extern
fun Sudoku_search(xss: board): stream(board)
//
(* ****** ****** *)

(* end of [midterm-2.dats] *)
