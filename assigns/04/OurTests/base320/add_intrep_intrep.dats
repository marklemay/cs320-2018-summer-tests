// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 320
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"


val() = println!("********** Testing on base 320 **********")
val() = println!("\n\n1) Add 0 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", add_intrep_intrep(intrep_make_int(0), intrep_make_int(0)))
val() = println!("\n\n2) Add 0 and 320\n\nExpected result: 0, 1\n")
val() = println!("Actual result:   ", add_intrep_intrep(intrep_make_int(0), intrep_make_int(320)))
val() = println!("\n\n3) Add 237 and 0\n\nExpected result: 237\n")
val() = println!("Actual result:   ", add_intrep_intrep(intrep_make_int(237), intrep_make_int(0)))
val() = println!("\n\n4) Add 40926165 and 60186291\n\nExpected result: 136, 136, 27, 3\n")
val() = println!("Actual result:   ", add_intrep_intrep(intrep_make_int(40926165), intrep_make_int(60186291)))
//val() = println!("\n\n5) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(5425234)), g0ofg1($list{int}(4325,2423,6,234))))
val() = println!()
val() = println!()