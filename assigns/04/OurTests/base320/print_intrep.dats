// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 320
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"

val() = println!()
val() = println!("********** Testing on base 320 **********")
val() = println!("\n\n1) Printing []\n\nExpected result: 0\n")
val() = println!("Actual result:   ", print_intrep(intrep_make_int(0)))
val() = println!("\n\n2) Printing [0, 1]\n\nExpected result: 320\n")
val() = println!("Actual result:   ", print_intrep(intrep_make_int(320)))
val() = println!("\n\n3) Printing [127, 166, 171, 65]\n\nExpected result: 2147483647\n")
val() = println!("Actual result:   ", print_intrep(intrep_make_int(2147483647)))
//val() = println!("\n\n4) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", print_intrep(g0ofg1($list{int}(533))))
val() = println!()
val() = println!()