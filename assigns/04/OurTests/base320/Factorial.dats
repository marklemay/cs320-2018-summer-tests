// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 320
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"


val() = println!()
val() = println!("********** Testing Factorial() on base 320 **********")
val() = println!("\n\n1) 0!\n\nExpected result: 1\n")
val() = println!("Actual result:   ", Factorial(0))
val() = println!("\n\n2) 1!\n\nExpected result: 1\n")
val() = println!("Actual result:   ", Factorial(1))
val() = println!("\n\n3) 30!\n\nExpected result: 0, 0, 0, 0, 20, 234, 49, 16, 53, 187, 146, 22, 230\n")
val() = println!("Actual result:   ", Factorial(30))
val() = println!("\n\n4) 320!\n\nExpected result: 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 145, 269, 206, 215, 199, 263, 230, 261, 112, 60, 184, 53, 276, 172, 35, 140, 212, 267, 122, 176, 175, 150, 65, 99, 296, 232, 249, 2, 178, 307, 200, 173, 144, 24, 206, 319, 105, 163, 19, 125, 161, 237, 23, 17, 303, 22, 52, 187, 148, 54, 157, 93, 199, 135, 244, 3, 245, 113, 319, 54, 211, 123, 2, 90, 232, 295, 301, 285, 154, 246, 167, 153, 297, 144, 83, 18, 235, 84, 45, 315, 17, 63, 144, 128, 135, 254, 268, 237, 175, 64, 313, 75, 234, 151, 36, 306, 158, 177, 251, 293, 18, 126, 280, 37, 222, 172, 62, 51, 243, 30, 291, 84, 260, 111, 279, 224, 260, 141, 48, 245, 174, 171, 294, 19, 99, 302, 219, 140, 162, 63, 244, 43, 269, 296, 90, 137, 147, 2, 79, 211, 15, 78, 35, 248, 194, 263, 212, 74, 230, 280, 86, 275, 5, 208, 21, 272, 277, 126, 283, 40, 18, 88, 91, 308, 110, 55, 304, 236, 239, 101, 10, 73, 312, 29, 51, 219, 240, 293, 236, 90, 38, 140, 173, 119, 231, 215, 237, 86, 305, 97, 180, 77, 67, 64, 304, 191, 80, 292, 254, 174, 235, 2, 289, 43, 279, 144, 138, 69, 138, 98, 184, 284, 2\n")
val() = println!("Actual result:   ", Factorial(320))
val() = println!("\n\n5) 1414!\n\nExpected result: ~~ Too long to include it here ~~\n")
val() = println!("Actual result:   ", Factorial(1414))
val() = println!()
val() = println!()