// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 2
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"


val() = println!()
val() = println!("********** Testing on binary **********")
val() = println!("\n\n1) Multiply 0 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_int_intrep(0, intrep_make_int(0)))
val() = println!("\n\n2) Multiply 0 and 320\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_int_intrep(0, intrep_make_int(320)))
val() = println!("\n\n3) Multiply 237 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_int_intrep(237, intrep_make_int(0)))
val() = println!("\n\n4) Multiply 314932 and 123175294\n\nExpected result: 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1\n")
val() = println!("Actual result:   ", mul_int_intrep(314932, intrep_make_int(123175294)))
val() = println!("\n\n5) Multiply 2147483647 and 1547922955\n\nExpected result: 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1\n")
val() = println!("Actual result:   ", mul_int_intrep(2147483647, intrep_make_int(1547922955)))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n6) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", mul_int_intrep(g0ofg1($list{int}(533)), g0ofg1($list{int}(6534,63,34,5))))
val() = println!()
val() = println!()