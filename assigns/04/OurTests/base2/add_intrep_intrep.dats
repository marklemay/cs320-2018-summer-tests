// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 2
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"


val() = println!("********** Testing on binary **********")
val() = println!("\n\n1) Add 0 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}()), g0ofg1($list{int}())))
val() = println!("\n\n2) Add 0 and 320\n\nExpected result: 0, 0, 0, 0, 0, 0, 1, 0, 1\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}()), g0ofg1($list{int}(0, 0, 0, 0, 0, 0, 1, 0, 1))))
val() = println!("\n\n3) Add 237 and 0\n\nExpected result: 1, 0, 1, 1, 0, 1, 1, 1\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(1, 0, 1, 1, 0, 1, 1, 1)), g0ofg1($list{int}())))
val() = println!("\n\n4) Add 40926165 and 60186291\n\nExpected result: 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1\n")
val() = println!("Actual result:   ", add_intrep_intrep(intrep_make_int(40926165), intrep_make_int(60186291)))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n5) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)), g0ofg1($list{int}(3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1))))
val() = println!()
val() = println!()