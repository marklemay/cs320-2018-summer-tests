// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 2
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"




val() = println!()
val() = println!("********** Testing on binary **********")
val() = println!("\n\n1) Coverting 0\n\nExpected result: \n")
val() = println!("Actual result:   ", intrep_make_int(0))
val() = println!("\n\n2) Coverting 1\n\nExpected result: 1\n")
val() = println!("Actual result:   ", intrep_make_int(1))
val() = println!("\n\n3) Coverting 271828\n\nExpected result: 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1\n")
val() = println!("Actual result:   ", intrep_make_int(271828))
val() = println!("\n\n4) Coverting 2147483647\n\nExpected result: 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1\n")
val() = println!("Actual result:   ", intrep_make_int(2147483647))
val() = println!()
val() = println!()



