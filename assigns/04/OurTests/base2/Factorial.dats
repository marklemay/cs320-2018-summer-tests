// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 2
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"


val() = println!()
val() = println!("********** Testing Factorial() on binary **********")
val() = println!("\n\n1) 0!\n\nExpected result: 1\n")
val() = println!("Actual result:   ", Factorial(0))
val() = println!("\n\n2) 1!\n\nExpected result: 1\n")
val() = println!("Actual result:   ", Factorial(1))
val() = println!("\n\n3) 30!\n\nExpected result: 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1\n")
val() = println!("Actual result:   ", Factorial(30))
val() = println!("\n\n4) 320!\n\nExpected result: ~~ Too long to include it here ~~\n")
val() = println!("Actual result:   ", Factorial(320))
val() = println!("\n\n5) 1414!\n\nExpected result: ~~ Too long to include it here ~~\n")
val() = println!("Actual result:   ", Factorial(1414))
val() = println!()
val() = println!()