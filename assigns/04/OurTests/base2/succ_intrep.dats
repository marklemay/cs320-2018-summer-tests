// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
#define BASE 2
// you will need to comment "#define BASE =..." in assign04.dats for this test to work
#include "./../../MySolution/assign04_sol.dats"


val() = println!()
val() = println!("********** Testing on binary **********")
val() = println!("\n\n1) Testing on []\n\nExpected result: 0\n")
val() = println!("Actual result:   ", succ_intrep(intrep_make_int(0)))
val() = println!("\n\n2) Testing on [0, 0, 0, 0, 0, 0, 1, 0, 1]\n\nExpected result: 1, 0, 0, 0, 0, 0, 1, 0, 1\n")
val() = println!("Actual result:   ", succ_intrep(intrep_make_int(320)))
val() = println!("\n\n3) Testing on [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n\nExpected result: 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1\n")
val() = println!("Actual result:   ", succ_intrep(intrep_make_int(2147483647)))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n4) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", succ_intrep(g0ofg1($list{int}(533))))
val() = println!()
val() = println!()