#include "./../MySolution/assign04_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing on decimal **********")
val() = println!("\n\n1) Printing []\n\nExpected result: 0\n")
val() = println!("Actual result:   ", print_intrep(intrep_make_int(0)))
val() = println!("\n\n2) Printing [0, 2, 3]\n\nExpected result: 320\n")
val() = println!("Actual result:   ", print_intrep(intrep_make_int(320)))
val() = println!("\n\n3) Printing [7, 4, 6, 3, 8, 4, 7, 4, 1, 2]\n\nExpected result: 2147483647\n")
val() = println!("Actual result:   ", print_intrep(intrep_make_int(2147483647)))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n4) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", print_intrep(g0ofg1($list{int}(533))))
val() = println!()
val() = println!()

//
val l3 = 1 :: nil0{int}()
val l6 = 4 :: 8 :: 9 :: nil0{int}()
val l8 = 8 :: 9 :: nil0{int}()

val () = println!("\n(1) = ", print_intrep(l3))
val () = println!("(4, 8, 9) = ", print_intrep(l6))
val () = println!("(8, 9) = ", print_intrep(l8))
val () = println!("() = ", print_intrep(nil0()))
//