#include "./../MySolution/assign04_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing on decimal **********")
val() = println!("\n\n1) Coverting 0\n\nExpected result: \n")
val() = println!("Actual result:   ", intrep_make_int(0))
val() = println!("\n\n2) Coverting 1\n\nExpected result: 1\n")
val() = println!("Actual result:   ", intrep_make_int(1))
val() = println!("\n\n3) Coverting 271828\n\nExpected result: 8, 2, 8, 1, 7, 2\n")
val() = println!("Actual result:   ", intrep_make_int(271828))
val() = println!("\n\n4) Coverting 2147483647\n\nExpected result: 7, 4, 6, 3, 8, 4, 7, 4, 1, 2\n")
val() = println!("Actual result:   ", intrep_make_int(2147483647))
val() = println!()
val() = println!()

val () = println!("\n0 = ", intrep_make_int(0))
val () = println!("439 = ", intrep_make_int(439))
val () = println!("3 = ", intrep_make_int(3))
val () = println!("34342 to intrep, should get 2, 4, 3, 4, 3: ", intrep_make_int(34342))

