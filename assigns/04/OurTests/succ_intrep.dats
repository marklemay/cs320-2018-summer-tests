#include "./../MySolution/assign04_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing on decimal **********")
val() = println!("\n\n1) Testing on []\n\nExpected result: 1\n")
val() = println!("Actual result:   ", succ_intrep(intrep_make_int(0)))
val() = println!("\n\n2) Testing on [0, 2, 3]\n\nExpected result: 1, 2, 3\n")
val() = println!("Actual result:   ", succ_intrep(intrep_make_int(320)))
val() = println!("\n\n3) Testing on [7, 4, 6, 3, 8, 4, 7, 4, 1, 2]\n\nExpected result: 8, 4, 6, 3, 8, 4, 7, 4, 1, 2\n")
val() = println!("Actual result:   ", succ_intrep(intrep_make_int(2147483647)))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n4) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", succ_intrep(g0ofg1($list{int}(533))))
val() = println!()
val() = println!()
