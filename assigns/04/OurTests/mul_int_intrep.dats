#include "./../MySolution/assign04_sol.dats"
// Multiply your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing on decimal **********")
val() = println!("\n\n1) Multiply 0 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_int_intrep(0, intrep_make_int(0)))
val() = println!("\n\n2) Multiply 0 and 320\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_int_intrep(0, intrep_make_int(320)))
val() = println!("\n\n3) Multiply 237 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_int_intrep(237, intrep_make_int(0)))
val() = println!("\n\n4) Multiply 341932 and 123175294\n\nExpected result: 8, 0, 0, 8, 2, 6, 4, 7, 5, 7, 1, 1, 2, 4\n")
val() = println!("Actual result:   ", mul_int_intrep(341932, intrep_make_int(123175294)))
val() = println!("\n\n5) Multiply 2147483647 and 1547922955\n\nExpected result: 5, 8, 8, 6, 1, 4, 8, 7, 6, 2, 3, 2, 9, 3, 1, 4, 2, 3, 3\n")
val() = println!("Actual result:   ", mul_int_intrep(2147483647, intrep_make_int(1547922955)))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n6) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", mul_int_intrep(g0ofg1($list{int}(533)), g0ofg1($list{int}(6534,63,34,5))))
val() = println!()
val() = println!()

