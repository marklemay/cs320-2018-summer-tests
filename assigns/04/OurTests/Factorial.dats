#include "./../MySolution/assign04_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing Factorial() on decimal **********")
val() = println!("\n\n1) 0!\n\nExpected result: 1\n")
val() = println!("Actual result:   ", Factorial(0))
val() = println!("\n\n2) 1!\n\nExpected result: 1\n")
val() = println!("Actual result:   ", Factorial(1))
val() = println!("\n\n3) 30!\n\nExpected result: 0, 0, 0, 0, 0, 0, 0, 8, 4, 8, 0, 3, 6, 3, 6, 8, 5, 0, 1, 9, 1, 2, 1, 8, 9, 5, 8, 2, 5, 2, 5, 6, 2\n")
val() = println!("Actual result:   ", Factorial(30))
val() = println!("\n\n4) 320!\n\nExpected result: ~~ Too long to include it here ~~\n")
val() = println!("Actual result:   ", Factorial(320))
val() = println!("\n\n5) 1414!\n\nExpected result: ~~ Too long to include it here ~~\n")
val() = println!("Actual result:   ", Factorial(1414))
val() = println!()
val() = println!()
