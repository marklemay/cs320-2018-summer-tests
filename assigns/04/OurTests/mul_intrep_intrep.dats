#include "./../MySolution/assign04_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing on decimal **********")
val() = println!("\n\n1) Multiply 0 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_intrep_intrep(intrep_make_int(0), intrep_make_int(0)))
val() = println!("\n\n2) Multiply 0 and 320\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_intrep_intrep(intrep_make_int(0), intrep_make_int(320)))
val() = println!("\n\n3) Multiply 237 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", mul_intrep_intrep(intrep_make_int(237), intrep_make_int(0)))
val() = println!("\n\n4) Multiply 341932 and 123175294\n\nExpected result: 8, 0, 0, 8, 2, 6, 4, 7, 5, 7, 1, 1, 2, 4\n")
val() = println!("Actual result:   ", mul_intrep_intrep(intrep_make_int(341932), intrep_make_int(123175294)))
val() = println!("\n\n5) Multiply 2147483647 and 1547922955\n\nExpected result: 5, 8, 8, 6, 1, 4, 8, 7, 6, 2, 3, 2, 9, 3, 1, 4, 2, 3, 3\n")
val() = println!("Actual result:   ", mul_intrep_intrep(intrep_make_int(2147483647), intrep_make_int(1547922955)))

#define :: list0_cons
val() = println!("\n\n6) Multiply 93846170483928194 and 749306371326\n\nExpected result: 4, 4, 2, 5, 6, 5, 4, 8, 4, 8, 4, 4, 0, 0, 4, 3, 5, 1, 8, 6, 4, 3, 3, 5, 9, 1, 3, 0, 7\n")
val() = println!("Actual result:   ", mul_intrep_intrep((4 :: 9 :: 1 :: 8 :: 2 :: 9 :: 3 :: 8 :: 4 :: 0 :: 7 :: 1 :: 6 :: 4 :: 8 :: 3 :: 9 :: nil0()), (6 :: 2 :: 3 :: 1 :: 7 :: 3 :: 6 :: 0 :: 3 :: 9 :: 4 :: 7 :: nil0())))

// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n6) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", mul_intrep_intrep(g0ofg1($list{int}(533)), g0ofg1($list{int}(6534,63,34,5))))
val() = println!()
val() = println!()

