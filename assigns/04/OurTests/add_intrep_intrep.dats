#include "./../MySolution/assign04_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val() = println!()
val() = println!("********** Testing on decimal **********")
val() = println!("\n\n1) Add 0 and 0\n\nExpected result: \n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}()), g0ofg1($list{int}())))
val() = println!("\n\n2) Add 0 and 320\n\nExpected result: 0, 2, 3\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}()), g0ofg1($list{int}(0, 2, 3))))
val() = println!("\n\n3) Add 237 and 0\n\nExpected result: 7, 3, 2\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(7, 3, 2)), g0ofg1($list{int}())))
val() = println!("\n\n4) Add 341932 and 123175294\n\nExpected result: 6, 2, 2, 7, 1, 5, 3, 2, 1\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(2, 3, 9, 1, 4, 3)), g0ofg1($list{int}(4, 9, 2, 5, 7, 1, 3, 2, 1))))
val() = println!("\n\n5) Add 2147483647 and 1547922955\n\nExpected result: 2, 0, 6, 6, 0, 4, 5, 9, 6, 3\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(7, 4, 6, 3, 8, 4, 7, 4, 1, 2)), g0ofg1($list{int}(5, 5, 9, 2, 2, 9, 7, 4, 5, 1))))
val() = println!("\n\n6) Add 98765432109876 and 1234567899876543\n\nExpected result: 9, 1, 4, 6, 8, 9, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1\n")
val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)), g0ofg1($list{int}(3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1))))
// it is optional (but nice) to throw exceptions on arguments out of scope
//val() = println!("\n\n7) Testing illegal arguments\n\nExpected result: !! Should get an exception !!\n")
//val() = println!("Actual result:   ", add_intrep_intrep(g0ofg1($list{int}(533)), g0ofg1($list{int}(6534,63,34,5))))
val() = println!()
val() = println!()


#define :: list0_cons

val l1 = 2 :: 1 :: nil0{int}()
val l2 = 9 :: 3 :: 4 :: nil0{int}()
val l3 = 1 :: nil0{int}()
val l4 = 2 :: nil0{int}()
val l5 = 7 :: nil0{int}()
val l6 = 4 :: 8 :: 9 :: nil0{int}()
val l7 = 4 :: 7 :: nil0{int}()
val l8 = 8 :: 9 :: nil0{int}()

val () = println!("12 + 439 = ", add_intrep_intrep(l1, l2))
val () = println!("1 + 2 = ", add_intrep_intrep(l3, l4))
val () = println!("7 + 984 = ", add_intrep_intrep(l5, l6))
val () = println!("74 + 98 = ", add_intrep_intrep(l7, l8))

(* ****** ****** *)

