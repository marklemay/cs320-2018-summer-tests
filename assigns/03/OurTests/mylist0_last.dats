#include "./../MySolution/assign03_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

// Does not need to test on an empty list.
val () = println!()
val () = println!("Testing on (2)\n\n Result should be 2\n Actual result is ", mylist0_last(list0_make_sing(2)))
val () = println!()
val () = println!()
val () = println!("Testing on range(213214)\n\n Result should be 213213\n Actual result is ", mylist0_last(list0_make_intrange(1, 213214)))
val () = println!()


// #define :: list0_cons

val l1 = 1 :: 34 :: 5 :: 34 :: 6 :: nil0{int}()

val l2 = 2 :: 7 :: 3 :: 7 :: nil0{int}()

val () = assertloc(mylist0_last(list0_make_intrange(1, 80001)) = 80000)
val () = assertloc(mylist0_last(l1) = 6)
val () = assertloc(mylist0_last(l2) = 7)

val l3 = list0_make_intrange(1, 80001)

val () = println!("Test last on list from 1 to 8000. Result: ", mylist0_last(l3))