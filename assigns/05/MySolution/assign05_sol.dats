(* ****** ****** *)
//
// HX:
// How to compile:
// myatscc assign05_sol.dats
//
// How to test it:
// ./assign05_sol_dats
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#include "./../assign05.dats"

(* ****** ****** *)

(*
Please implement
the following functions:

fun
stream_ln2(): stream(double)

fun
intpair_enumerate((*void*)): stream(int2)

fun
EulerTrans(xs: stream(double)): stream(double)
*)

(* ****** ****** *)

(* end of [assign05_sol.dats] *)
