#include "./../MySolution/assign05_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

val() = println!("\n================================================================================\n================================================================================\n|||||||||\t\t      Testing stream_ln2()\t\t       |||||||||\n================================================================================\n================================================================================\n\n")

fun test_n(n:int) =
  let
    fun helper(n:int, count:int, stream:stream(double)) =
      if n > count
      then
        let
          val- stream_cons(l, ln) = !stream
        in
          // if n = count+1                                       // Uncomment to print only the nthe partial sum
          // then println!(count+1, "th partial sum is ", l)      // Uncomment to print only the nthe partial sum
          // else helper(n, count+1, ln)                          // Uncomment to print only the nthe partial sum

          println!((count+1), "th partial sum is ", l);           // Print all partial sum
          helper(n, count+1, ln)                                  // Print all partial sum
        end
      else ()
  in
    helper(n, 0, stream_ln2())
  end

val() = test_n(9000) (* Swap the snippet in test_n() to print all partial sum / only the last partial sum *)

(*

Type the following into WolframAlpha and replace n to check the nth partial sum.

"sum from x = 1 to n of (-1)^(x+1)*(1/x)"

*)

val() = println!() // Just for aesthetic
