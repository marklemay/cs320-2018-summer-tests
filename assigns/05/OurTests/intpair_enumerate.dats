#include "./../MySolution/assign05_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

val() = println!("\n================================================================================\n================================================================================\n|||||||||\t\t    Testing intpair_enumerate()\t\t       |||||||||\n================================================================================\n================================================================================\n\n")

fun test_n(n:int) =
  let
    fun helper(n:int, count:int, stream:stream(int2)) =
      if n > count
      then
        let
          val- stream_cons(t, nt) = !stream
        in
          // if n = count+1                                                   // Uncomment to show the last tuple
          // then println!((count+1), "th pair is (", t.0, ", ", t.1, ")")    // Uncomment to show the last tuple
          // else helper(n, count+1, nt)                                      // Uncomment to show the last tuple

          println!((count+1), "th pair is (", t.0, ", ", t.1, ")");           // Print all generated tuples
          helper(n, count+1, nt)                                              // Print all generated tuples
        end
      else ()
  in
    helper(n, 0, intpair_enumerate())
  end

(* Swap the snippet in test_n() to print all generated tuples / the last tuple *)


// these comments suggest one possible ordering of intpair_enumerate() any but any order would be correct
val() = test_n(15) // The last one might be (4, 4)
val() = println!()
val() = println!()
val() = test_n(66) // The last one might be (10, 10)
val() = println!()
val() = println!()
val() = test_n(320) // The last one might be (19, 24)
val() = println!()
val() = println!()
val() = test_n(51681) // The last one might be (320, 320)
val() = println!()


//Hint: a nice way to test streams is to take a large prefix, and filter by the property it should support
fun {A:t@ype}stream_take(s:stream(A),n:int):stream(A) =
  $delay(
    if n <= 0
    then stream_nil()
    else case+ !s of
         | stream_nil()         => stream_nil()
         | stream_cons(a, rest) => stream_cons(a, stream_take(rest, n-1))
  )

val bad_things = stream_filter_fun(stream_take<int2>(intpair_enumerate(), 1000), lam t => t.0 > t.1)

val () = assertloc(stream_is_nil(bad_things)) //shouldn't have bad things


//Hint: can check that somthing exists in a stream, may run forever if it is not found
fun {A:t@ype}stream_exists(s:stream(A), p: A -<cloref1> bool):bool =
  case+ !s of
  | stream_nil()         => false
  | stream_cons(a, rest) =>
      if p(a)
      then true
      else stream_exists(rest, p)

val () = assertloc(stream_exists<int2>(intpair_enumerate(), lam t => (t.0 = 320) && (t.1 = 320))) //should have a good thing

val () = assertloc(stream_exists<int2>(intpair_enumerate(), lam t => (t.0 = 0) && (t.1 = 0))) // "0 is the first natural number"
val () = assertloc(stream_exists<int2>(intpair_enumerate(), lam t => (t.0 = 2) && (t.1 = 5))) // 2 < 5