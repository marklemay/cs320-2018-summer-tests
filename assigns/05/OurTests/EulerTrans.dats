#include "./../MySolution/assign05_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

val() = println!("\n================================================================================\n================================================================================\n|||||||||\t\t    Testing EulerTrans()\t\t       |||||||||\n================================================================================\n================================================================================\n\n")

fun stream_x(n:double):stream(double) =      // Generate a stream of 1, 1/2, 1/4, 1/8, ...
  $delay stream_cons(n, stream_x(n/2.0))  // Can change to generate other stream as an input of EulerTrans()

val input = stream_x(1.0)

val() =
  let
    fun loop(n:int, count:int, stream:stream(double)) =
      if n > count
      then
        let
          val- stream_cons(x, xs) = !stream
        in
          if count = 0
          then (print(x);loop(n, count+1, xs))
          else (print(", ");print(x);loop(n, count+1, xs))
        end
  in
    print("Stream of x is ");
    loop(5, 0, input);
    print(", ...\n\n")
  end

fun test_n(n:int) =
  let
    fun helper(n:int, count:int, stream:stream(double)) =
      if n > count
      then
        let
          val- stream_cons(e, el) = !stream
        in
          // if n = count+1                                     // Uncomment to show the nth of the stream y
          // then print(e)                                      // Uncomment to show the nth of the stream y
          // else helper(n, count+1, el)                        // Uncomment to show the nth of the stream y

          if count = 0                                          // Print the first n elements of the stream y
          then (print(e);helper(n, count+1, el))                // Print the first n elements of the stream y
          else (print(", ");print(e);helper(n, count+1, el))    // Print the first n elements of the stream y
        end
      else ()
  in
    helper(n, 0, EulerTrans(input))
  end

val() = (print("Stream of y is ");test_n(5);print(", ...\n\n"))
val() = print("Expected result is 0.75, 0.875, 0.9375, 0.96875, 0.984375, ... if the input stream is 1.0, 0.5, 0.25, 0.125, 0.0625, ...")

(* ****************** *)

// Swap the snippet in test_n() before using the code below

(* ****************** *)

// val() = print("~~~~~~~~~~~~\t   Testing on nth of the output stream (y)     \t~~~~~~~~~~~~\n\n*** The provided expected result is based on a input of 1.0, 0.5, 0.25, 0.125, 0.0625, ... ***\n\n")
// val() = (print("The 6th of stream y is ");test_n(6);print(" (Expected 0.992188)\n"))
// val() = (print("The 8th of stream y is ");test_n(8);print(" (Expected 0.998047)\n"))
// val() = (print("The 10th of stream y is ");test_n(10);print(" (Expected 0.999512)"))
val() = print("\n\n")
