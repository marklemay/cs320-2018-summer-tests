(* ****** ****** *)
//
// G2-2: 20 points
//
// Solving Sudoku based on depth-first search
//
// Sudoku is a popular game:
// http://www.7sudoku.com/view-puzzle?date=20171124
//
// A Sudoku board is a 3-by-3 matrix of 3-by-3
// matrices of integer digits. Note that an entry
// is considered blank if the digit 0 is stored
// in it.
//
// A partial board is one that may contain digit 0.
// For instance, the following board is partial:
//
(* ****** ****** *)
(*
-----------------
     |     |  7 5 
  4  |9 8  |      
  8  |     |      
-----------------
3   2|  1  |5     
9 1  |7    |      
     |    2|    6 
-----------------
     |  6 4|    1 
     |  2  |6     
4    |    5|    2 
-----------------
*)
//
// A full board is one that contains only non-zero
// digits. For instance, the following board is full:
//
(*
-----------------
1 2 9|6 4 3|8 7 5 
5 4 7|9 8 1|2 6 3 
6 8 3|2 5 7|1 4 9 
-----------------
3 6 2|4 1 8|5 9 7 
9 1 5|7 3 6|4 2 8 
8 7 4|5 9 2|3 1 6 
-----------------
2 9 8|3 6 4|7 5 1 
7 5 1|8 2 9|6 3 4 
4 3 6|1 7 5|9 8 2 
-----------------
*)
(* ****** ****** *)
//
// A board extends another one if
// the former can be obtained by changing
// some of the zero entries in the latter
// into non-zero entries.
//
// The function Sudoku_search takes
// a partial board and returns a stream of
// *all* full boards that extend the partial
// board.
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
typedef
board =
matrix0(matrix0(int))
//
extern
fun Sudoku_search_opt(xss: board): Option(board)
extern
fun Sudoku_search_all(xss: board): stream(board)
//
(* ****** ****** *)

(* end of [Sudoku_search.dats] *)
