(* ****** ****** *)
//
#include
"./../Sudoku_search.dats"
//
(* ****** ****** *)

typedef mcell = matrix0(int)

(* ****** ****** *)

(*
fun
{a:vt0p}
matrix0_foreach
(
  M : matrix0(a)
, fwork: (&a >> _) -<cloref1> void
) : void // end of [matrix0_foreach]
*)
extern
fun
{a:t0p}
matrix0_forall
(
M : matrix0(a), ftest: (a) -<cloref1> bool
) : bool // end of [matrix0_forall]

exception False of ()

implement
{a}
matrix0_forall
  (M, ftest) =
(
try let
  val () =
  matrix0_foreach(M, fwork) in true
end with
  | ~False () => false
) where
{
  val
  fwork =
  lam(x: &a >> _): void =<cloref1> if ~ftest(x) then $raise False() else ()
} (* end of [matrix0_forall] *)

(* ****** ****** *)

fun
board_is_full
  (xss: board): bool = let
//
fun
mcell_is_full(xs: matrix0(int)): bool =
  matrix0_forall<int>(xs, lam(x) => x != 0)
//
in
  matrix0_forall<mcell>(xss, lam(xs) => mcell_is_full(xs))
end

(* ****** ****** *)

#define 
GRAPHSEARCH_DFS 1 // for using DFS

(* ****** ****** *)

#include
"./node_modules/atscntrb-bucs320-graphsearch/mylibies.hats"

(* ****** ****** *)

assume $GraphSearch.node_type = board
assume $GraphSearch.nodelst_vtype = list0(board)

(* ****** ****** *)

typedef
node =
$GraphSearch.node

exception Found of node

(* ****** ****** *)

#staload DFS = $GraphSearch_dfs

(* ****** ****** *)

implement
Sudoku_search_opt(xss) =
(
try let
  val () =
  $DFS.GraphSearch_dfs(store)
in
  None() // no solution
end with
  | ~Found(nx) => Some(nx)
) where
{
val
store =
slistref_make_nil{node}()
//
val () =
slistref_insert(store, xss)
//
} (* end of [Sudoku_search_opt] *)

(* ****** ****** *)

(* end of [Sudoku_search_sol.dats] *)
