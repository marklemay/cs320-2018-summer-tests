#include "./../MySolution/assign01_sol.dats"


val () = assertloc(sumup_3_5(0) = 0)

val () = assertloc(sumup_3_5(3) = 0) // since all the numbers less than 3 (0,1,2) do not meet the condition.

val () = assertloc(sumup_3_5(15) = 3+6+9+12+5+10)

val () = assertloc(sumup_3_5(16) = 3+6+9+12+5+10)