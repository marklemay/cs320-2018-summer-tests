(* ****** ****** *)
//
// HX:
// How to compile:
// myatscc assign02_sol.dats
//
// How to test it:
// ./assign02_sol_dats
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#include "./../assign02.dats"

(* ****** ****** *)

(*
implement min_val(xs) = ...
*)

(* ****** ****** *)

(*
implement remove_spaces(xs) = ...
*)

(* ****** ****** *)

(*
implement encipher(xs, shift) = ...
implement decipher(xs, shift) = ...
*)

(* ****** ****** *)

(*

implement
myslice(xs, start, finish) = ...

*)

(* ****** ****** *)

(* end of [assign02_sol.dats] *)
