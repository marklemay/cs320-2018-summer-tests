(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#include "./../../mylib/class.dats"
//
(*
#include "./../../mylib/myclass.dats"
*)
//
(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

val () =
int_foreach<>(10, lam(i) => println!("i = ", i))

(* ****** ****** *)

(* end of [myclass.dats] *)
