(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
extern
fun
{a:t@ype}
mergesort(xs: list0(a)): list0(a)
//
(* ****** ****** *)

(*
implement
{a}
mergesort
  (xs) = let
//
  val (xs1, xs2) = split(xs)
  val ys1 = mergesort<a>(xs1)
  val ys2 = mergesort<a>(xs2)
//
in
  merge(ys1, ys2)
end where
{
  extern
  fun
  split : list0(a) -> (list0(a), list0(a))
  extern
  fun
  merge : (list0(a), list0(a)) -> list0(a)
}
*)

(* ****** ****** *)

implement
{a}
mergesort
  (xs) =
  sort(xs) where
{
fun
split
(
xs: list0(a)
) : $tup(list0(a), list0(a)) =
(
case+ xs of
| nil0() => $tup(nil0, nil0)
| cons0(x1, xs) =>
  (
    case+ xs of
    | nil0() =>
      $tup(list0_sing(x1), nil0)
    | cons0(x2, xs) => let
        val $tup(xs1, xs2) = split(xs)
      in
        $tup(cons0(x1, xs1), cons0(x2, xs2))
      end
  )
)
fun
merge
(xs: list0(a), ys: list0(a)): list0(a) =
(
case+ (xs, ys) of
| (nil0(), _) => ys
| (_, nil0()) => xs
| (cons0(x, xs2), cons0(y, ys2)) =>
  (
    if
    (cmp(x, y) <= 0)
    then
      cons0(x, merge(xs2, ys))
    else
      cons0(y, merge(xs, ys2))
    // end of [if]
  ) where
  {
    val cmp = gcompare_val_val<a>
  }
)

fun
sort
(
xs: list0(a)
) : list0(a) = let
  val n = length(xs)
in
  if
  (n < 2)
  then xs
  else let
//
  val $tup(xs1, xs2) = split(xs)
  val ys1 = sort(xs1)
  val ys2 = sort(xs2)
//
  in
    merge(ys1, ys2)
  end
end
} (* end of [mergesort] *)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

val xs =
g0ofg1($list{int}(3, 5, 2, 1, 8, 8, 2, 9, 7))
val () = println! ("xs = ", xs)
val xs = println! ("sort(xs) = ", mergesort<int>(xs))

(* ****** ****** *)

local

implement
gcompare_val_val<int>(x, y) = compare(y, x)

in

val ys =
g0ofg1($list{int}(3, 5, 2, 1, 8, 8, 2, 9, 7))
val () = println! ("ys = ", ys)
val ys = println! ("sort(ys) = ", mergesort<int>(ys))

end

(* ****** ****** *)

(* end of [mergesort.dats] *)
