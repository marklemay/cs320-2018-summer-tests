(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "libats/libc/SATS/math.sats"
#staload _ = "libats/libc/DATS/math.dats"

(* ****** ****** *)

extern
fun
{a:t@ype}
tabulate
(fopr: int -<cloref1> a): stream(a)

implement
{a}//tmp
tabulate(fopr) = aux(0) where
{
  fun
  aux(n: int): stream(a) =
  $delay(stream_cons(fopr(n), aux(n+1)))
}

val theNats = tabulate<int>(lam(n) => n)
val theSquares = tabulate<int>(lam(n) => n*n)
val theCubes = tabulate<int>(lam(n) => n*n*n)

(* ****** ****** *)

extern
fun
{a:t@ype}
seq2series
(xs: stream(a)): stream(a)

(* ****** ****** *)

implement
{a}
seq2series(xs) =
aux(xs, gnumber_int<a>(0)) where
{
fun
aux
( xs:
  stream(a)
, state0: a): stream(a) = $delay
(
case- !xs of
| stream_cons(x, xs) =>
  let
    val state1 =
      gadd_val_val<a>(state0, x)
  in
    stream_cons(state1, aux(xs, state1))
  end // end of [stream_cons]
)
}

(* ****** ****** *)

val recips =
tabulate<double>(lam(n) => 1.0 / (n+1))
val harmonies = seq2series<double>(recips)

(* ****** ****** *)

val N = 1000000
val () = println! ("harmonies[", N, "] = ", harmonies[N]) 
val () = println! ("log(", N, ") = ", log(1.0*N))

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [series.dats] *)
