(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

(*
#staload "libats/libc/SATS/math.sats"
#staload _ = "libats/libc/DATS/math.dats"
*)

(* ****** ****** *)

extern
fun
{a:t@ype}
tabulate
(fopr: int -<cloref1> a): stream(a)

implement
{a}//tmp
tabulate(fopr) =
stream_tabulate_cloref<a>(fopr)

(* ****** ****** *)

typedef
int2 = $tup(int, int)

(* ****** ****** *)

(*
//
// This solution does not work!!!
//
fun
f2denum
(i: int): stream(int2) = $delay
(
let
  val rows = f2denum(i+1)
  val row0 = tabulate<int2>(lam(n) => $tup(i, n))
in
  !(stream_append<int2>(row0, rows))
end
)

val
xys = f2denum(0)
val () =
println! ("xys[0] = ", xy.0, "; ", xy.1) where {
  val xy = xys[0]
}
val () =
println! ("xys[1] = ", xy.0, "; ", xy.1) where {
  val xy = xys[1]
}

*)

(* ****** ****** *)

fun
diagnal(n: int): stream(int2) =
aux(0) where
{
  fun aux(i: int): stream(int2) = $delay
  (
    if i <= n
    then stream_cons($tup(i, n-i), aux(i+1))
    else stream_nil()
  )
}

(* ****** ****** *)

val xys =
stream_concat<int2>
(
tabulate<stream(int2)>(lam(n) => diagnal(n))
)

val () =
println! ("xys[0] = ", xy.0, "; ", xy.1) where {
  val xy = xys[0]
}
val () =
println! ("xys[1] = ", xy.0, "; ", xy.1) where {
  val xy = xys[1]
}

val () =
println! ("xys[2] = ", xy.0, "; ", xy.1) where {
  val xy = xys[2]
}
val () =
println! ("xys[3] = ", xy.0, "; ", xy.1) where {
  val xy = xys[3]
}

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [2D-enum.dats] *)
