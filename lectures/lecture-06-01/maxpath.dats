(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

#define ATS_PACKNAME "MAXPATH"

(* ****** ****** *)

typedef layer = list0(int)
typedef triangle = list0(layer)

(* ****** ****** *)

val layer1 = g0ofg1($list{int}(3))
val layer2 = g0ofg1($list{int}(2, 5))
val layer3 = g0ofg1($list{int}(100, 1, 99))
val layer4 = g0ofg1($list{int}(2, 2, 3, 5))
val layer5 = g0ofg1($list{int}(1, 2, 3, 4, 5))

val triangle =
g0ofg1($list{layer}(layer1, layer2, layer3, layer4, layer5))

(* ****** ****** *)

extern
fun
triangle_subleft(triangle): triangle
and
triangle_subright(triangle): triangle

(* ****** ****** *)

implement
triangle_subleft(tr0) =
(
case- tr0 of
cons0(_, ls) =>
list0_map<layer><layer>(ls, lam(l) => init(l))
) where
{
  fun init(xs: layer): layer =
    (
      case- xs of
      | cons0(x, nil0()) => nil0()
      | cons0(x, xs) => cons0(x, init(xs))
    )
}

implement
triangle_subright(tr0) =
(
case- tr0 of
cons0(_, ls) =>
list0_map<layer><layer>(ls, lam(l) => tail(l))
)

(* ****** ****** *)

typedef path = list0(int)

extern
fun maxpath(triangle): $tup(int, path)

(* ****** ****** *)

implement
maxpath(tr0) =
(
case- tr0 of
| cons0(l1, ls2) =>
  (
    case ls2 of
    | nil0() => $tup(head(l1), l1)
    | cons0(_, _) => let
        val trl = triangle_subleft(tr0)
        and trr = triangle_subright(tr0)
        val resl = maxpath(trl) and resr = maxpath(trr)
      in
        if
        (resl.0 > resr.0)
        then
        $tup(head(l1)+resl.0, cons0(head(l1), resl.1))
        else
        $tup(head(l1)+resr.0, cons0(head(l1), resr.1))
      end
  )
)

(* ****** ****** *)

val res0 = maxpath(triangle)
val () = println! ("max = ", res0.0)
val () = println! ("path = ", res0.1)

(* ****** ****** *)

(* end of [maxpath.dats] *)
