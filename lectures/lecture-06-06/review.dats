(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/class.dats"

(* ****** ****** *)

extern
fun
{a:t@ype}
mylist0_filter
( xs: list0(a)
, test: (a) -<cloref1> bool): list0(a)

(* ****** ****** *)

implement
{a}
mylist0_filter
(xs, test) = auxlst(xs) where
{
  fun
  auxlst(xs: list0(a)): list0(a) =
  (
    case+ xs of
    | nil0() => nil0()
    | cons0(x, xs) =>
      if test(x)
        then cons0(x, auxlst(xs)) else auxlst(xs)
      // end of [if]
  )
}

(* ****** ****** *)

implement
{a}
mylist0_filter
(xs, test) =
list0_reverse<a>
(
list0_foldleft<res><a>
(
  xs, nil0()
, lam(res, x) =>
  if test(x) then cons0(x, res) else res
) where
{
  typedef res = list0(a)
}
)

(* ****** ****** *)

extern
fun
{a:t@ype}
mylist0_find_exn
( xs: list0(a)
, test: (a) -<cloref1> bool): (a)
extern
fun
{a:t@ype}
mylist0_find_opt
( xs: list0(a)
, test: (a) -<cloref1> bool): option0(a)

(* ****** ****** *)

implement
{a}
mylist0_find_exn
  (xs, test) = let
//
val opt =
mylist0_find_opt<a>(xs, test)
//
in
  case+ opt of
  | None0() =>
    $raise NotFoundExn()
  | Some0(x) => x
end // end of [mylist0_find_exn]

implement
{a}
mylist0_find_opt
  (xs, test) = let
//
typedef res = option0(a)
//
exception Found of (a)
//
in

try
list0_foldleft<res><a>
(
  xs, None0()
, lam(res, x) =>
  if test(x) then $raise Found(x) else res
)
with ~Found(x) => Some0(x)

end // end of [mylist0_find_opt]

(* ****** ****** *)

extern
fun
mylist0_mean(list0(double)): double

(*
implement
mylist0_mean(xs) = let
//
val () = assertloc(isneqz(xs))
//
typedef a = double and res = double
//
val tally =
list0_foldleft<res><a>(xs, 0.0, lam(res, x) => res+x)
//
in
  tally/length(xs)
end // end of [mylist0_mean]
*)

implement
mylist0_mean(xs) = let
//
val () = assertloc(isneqz(xs))
//
typedef a = double and res = $tup(int, double)
//
val res =
list0_foldleft<res><a>
(xs, $tup(0, 0.0), lam(res, x) => $tup(res.0+1, res.1+x))
//
in
  res.1 / res.0
end // end of [mylist0_mean]

(* ****** ****** *)

(* end of [review.dats] *)
