(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

(*
array0(a): for arrays of a's
*)

(* ****** ****** *)

val A0 = array0_make_elt<int>(10, 0)
val A1 = array0_tabulate<int>(i2sz(10), lam(i) => sz2i(i*i))
val () = println! ("A0 = ", A0)
val () = println! ("A1 = ", A1)
val () = A0[5] := 1
val () = A0[6] := 1
val () = A0[10] := 1
val () = println! ("A0 = ", A0)
val tally =
array0_foldleft<int><int>(A0, 0, lam(res, x) => res + x)
val () = println! ("tally = ", tally)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)