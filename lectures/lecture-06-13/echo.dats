(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

fun
echo() = let
  fun
    loop(xs: stream(string)): void =
    (
      case+ !xs of
      | stream_nil() => ()
      | stream_cons(x, xs) => (println!(x); loop(xs))
    )
  val lines = stream_vt2t(streamize_fileref_line(stdin_ref))
  val lines = stream_filter<string>(lines, lam(x) => isneqz(x))
in
    loop(lines)
end //  end of [echo]

(* ****** ****** *)

val Hello = "Hello"
val () = println! ("|Hello| = ", string_length(Hello))

(* ****** ****** *)

val Hello2 =
stream_vt2t
(
streamize_string_char(Hello)
)
val () = println! ("|Hello2| = ", stream_length(Hello2))

(* ****** ****** *)

implement main0() = () // echo()

(* ****** ****** *)

(* end of [echo.dats] *)
