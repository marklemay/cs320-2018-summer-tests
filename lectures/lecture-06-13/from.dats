(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(*
fun
from
(n: int): list0(int) =
list0_cons(n, from(n+1))
*)
fun
from
(n: int): stream(int) =
$delay
(stream_cons(n, from(n+1)))

val xs = from(0)
val ys = stream_map_cloref<int><int>(xs, lam(x) => x * x)

val-stream_cons(x, xs) = !xs
val () = println! ("x = ", x)
// val () = println! ("xs = ", xs)

val-stream_cons(x, xs) = !xs
val () = println! ("x = ", x)
// val () = println! ("xs = ", xs)

val-stream_cons(x, xs) = !xs
val () = println! ("x = ", x)
// val () = println! ("xs = ", xs)

val () = println! ("ys[0] = ", ys[0])
val () = println! ("ys[1] = ", ys[1])
val () = println! ("ys[2] = ", ys[2])
val () = println! ("ys[3] = ", ys[3])

(* ****** ****** *)

(* end of [from.dats] *)
