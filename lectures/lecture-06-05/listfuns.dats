(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

extern
fun
tally : list0(int) -> int

(* ****** ****** *)

(*
// This is O(n)-time
*)
implement
tally(xs) =
list0_foldleft<int><int>
(xs, 0, lam(res, x) => res + x)

(* ****** ****** *)

(*
implement
tally(xs) =
(
case+ xs of
| nil0() => 0
| cons0(x, xs) => x + tally(xs)
)
*)
(*
implement
tally(xs) = loop(xs, 0) where
{
fun
loop
(xs: list0(int), res: int): int =
case+ xs of
| nil0() => res
| cons0(x, xs) => loop(xs, res + x)
}
*)

(* ****** ****** *)

datatype
option0(a:t@ype) =
  | None0 of ()
  | Some0 of (a)

(* ****** ****** *)

extern
fun
{a:t@ype}
mylist0_nth:
(list0(a), int) -> a

implement
{a}
mylist0_nth
  (xs, n) =
(
case+ xs of
| nil0() =>
  (
    $raise ListSubscriptExn()
  )
| cons0(x, xs) =>
  if n > 0
    then mylist0_nth<a>(xs, n-1) else x
  // end of [if]
)

(* ****** ****** *)

extern
fun
{a:t@ype}
mylist0_nth_opt :
(list0(a), int) -> option0(a)

(* ****** ****** *)

implement
{a}
mylist0_nth_opt
  (xs, n) =
(
case+ xs of
| nil0() => None0()
| cons0(x, xs) =>
  if n > 0
    then mylist0_nth_opt<a>(xs, n-1) else Some0(x)
  // end of [if]
)

(* ****** ****** *)

(*
implement
tally(xs) =
loop(0, 0) where
{
//
val n = length(xs)
//
fun
loop
(i: int, res: int): int =
if i < n then loop(i+1, xs[i] + res) else res
//
} (* end of [tally] *)
*)

(* ****** ****** *)

val xs =
g0ofg1($list{int}(1,2))

val () =
println!("xs[0] = ", mylist0_nth<int>(xs, 0))
val () =
println!("xs[1] = ", mylist0_nth<int>(xs, 1))
val () =
println!("xs[2] = ", mylist0_nth<int>(xs, 2))

(* ****** ****** *)

(* end of [listfuns.dats] *)
