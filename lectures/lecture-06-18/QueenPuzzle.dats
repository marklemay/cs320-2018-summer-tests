macdef N = 8

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

#staload "./DFS.dats"

#include
"./../../mylib/class.dats"

assume node = list0(int)

implement
{}
children(nx0) = let
//
val nxs =
int_list0_map<node>
(N, lam(i) => list0_cons(i, nx0))
//
in
  list0_filter<node>(nxs, lam(nx) => test(nx))
end where
{
  fun
  test(nx1: node): bool = let
    val-list0_cons(n0, nx) = nx1
  in
    list0_iforall<int>(nx, lam(i, n) => (n0 != n) && abs(n0 - n) != i+1)
  end
}

implement
main0() = () where
{
val
theSolutions_par =
depth_first_search<>(list0_nil())
val
theSolutions_ful =
list0_filter<node>
(theSolutions_par, lam(nx) => length(nx) = N)
//
val () = println! ("|theSolutions_par| = ", length(theSolutions_par))
val () = println! ("|theSolutions_ful| = ", length(theSolutions_ful))
//
} (* end of [main0] *)