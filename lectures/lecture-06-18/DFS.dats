#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

abstype node = ptr
typedef nodes = list0(node)

extern
fun{}
children : node -> nodes

extern
fun{}
depth_first_search(node): nodes

implement
{}
depth_first_search
  (nx0) = let
//
val nxs = children(nx0)
val nxss = list0_map<node><nodes>(nxs, lam(nx) => depth_first_search(nx))
//
in
   list0_cons(nx0, list0_concat<node>(nxss))
end // end of [depth_first_search]
