(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
(*
http://rosettacode.org/wiki/Ternary_logic
*)
(* ****** ****** *)

datatype trit =
  | True | False | Maybe

(* ****** ****** *)

extern
fun
trit_not(x: trit): trit

extern
fun
trit_or(x: trit, y: trit): trit

extern
fun
trit_and(x: trit, y: trit): trit

(* ****** ****** *)

(* end of [extra07.dats] *)
