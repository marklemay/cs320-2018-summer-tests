(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
(*
Please solve the following problem:
http://rosettacode.org/wiki/Floyd%27s_triangle
*)
(* ****** ****** *)

extern
fun SolveIt(): void

(* ****** ****** *)
//
// Please give your implemenation of SolveIt as follows:
//
(* ****** ****** *)

(* end of [extra04.dats] *)
