#include "./../extra01.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

val() = assertloc(list0_forall2(SolveIt(), g0ofg1($list{int}(2011, 2016, 2022, 2033, 2039, 2044, 2050, 2061, 2067, 2072, 2078, 2089, 2095, 2101, 2107, 2112, 2118)), lam(x, y) => x=y))  // Nothing should happen
