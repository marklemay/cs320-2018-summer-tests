#include "./../extra03.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.


val() = println!("\n1) Testing on an empty data\n\nExpected result: \nActual result:   ", gflatten(glist_cons{int}(gitem_list(glist_nil()), glist_nil())), "\n\n2) Testing on a non-empty data\n\nExpected result: 320, 8, 237, 555\nActual result:   ", gflatten(glist_cons{int}(gitem_list{int}(glist_cons{int}(gitem_elt{int}(320), glist_nil())), glist_cons{int}(gitem_elt{int}(8), glist_cons{int}(gitem_elt{int}(237), glist_cons{int}(gitem_list{int}(glist_cons(gitem_elt{int}(555), glist_nil())), glist_nil()))))), "\n\n3) Testing on a non-empty data\n\nExpected result: ATS, is, very, FASCINATING\nActual result:   ", gflatten(glist_cons{string}(gitem_list{string}(glist_cons{string}(gitem_elt{string}("ATS"), glist_nil())), glist_cons{string}(gitem_elt{string}("is"), glist_cons{string}(gitem_elt{string}("very"), glist_cons{string}(gitem_list{string}(glist_cons(gitem_elt{string}("FASCINATING"), glist_nil())), glist_nil()))))), "\n")

//
#define :: glist_cons
#define elt gitem_elt

val genlist = 

elt(4) :: elt(5) :: gitem_list((elt(4) :: elt(2) :: elt(1) :: glist_nil{int}())) ::
  elt(4) :: elt(9) :: glist_nil{int}()

val () = println!("Should get (4, 5, 4, 2, 1, 4, 9). ", gflatten<int>(genlist))