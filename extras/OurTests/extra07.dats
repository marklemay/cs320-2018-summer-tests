#include "./../extra07.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

fun trit_to_string(x:trit):string =
  case+ x of
  | True() => "True"
  | False() => "False"
  | Maybe() => "Maybe"

val() = println!("\n1) Not True\n\nExpected result: False\nActual result:   ", trit_to_string(trit_not(True())), "\n\n2) Not False\n\nExpected result: True\nActual result:   ", trit_to_string(trit_not(False())), "\n\n3) Not Maybe\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_not(Maybe())),  "\n\n4) True and True\n\nExpected result: True\nActual result:   ", trit_to_string(trit_and(True(), True())), "\n\n5) True and False\n\nExpected result: False\nActual result:   ", trit_to_string(trit_and(True(), False())), "\n\n6) False and True\n\nExpected result: False\nActual result:   ", trit_to_string(trit_and(False(), True())), "\n\n7) False and False\n\nExpected result: False\nActual result:   ", trit_to_string(trit_and(False(), False())), "\n\n8) True and Maybe\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_and(True(), Maybe())), "\n\n9) Maybe and True\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_and(Maybe(), True())), "\n\n10) False and Maybe\n\nExpected result: False\nActual result:   ", trit_to_string(trit_and(False(), Maybe())), "\n\n11) Maybe and False\n\nExpected result: False\nActual result:   ", trit_to_string(trit_and(Maybe(), False())), "\n\n12) Maybe and Maybe\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_and(Maybe(), Maybe())), "\n\n13) True or True\n\nExpected result: True\nActual result:   ", trit_to_string(trit_or(True(), True())), "\n\n14) True or False\n\nExpected result: True\nActual result:   ", trit_to_string(trit_or(True(), False())), "\n\n15) False or True\n\nExpected result: True\nActual result:   ", trit_to_string(trit_or(False(), True())), "\n\n16) False or False\n\nExpected result: False\nActual result:   ", trit_to_string(trit_or(False(), False())), "\n\n17) True or Maybe\n\nExpected result: True\nActual result:   ", trit_to_string(trit_or(True(), Maybe())), "\n\n18) Maybe or True\n\nExpected result: True\nActual result:   ", trit_to_string(trit_or(Maybe(), True())), "\n\n19) False or Maybe\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_or(False(), Maybe())), "\n\n20) Maybe or False\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_or(Maybe(), False())), "\n\n21) Maybe or Maybe\n\nExpected result: Maybe\nActual result:   ", trit_to_string(trit_or(Maybe(), Maybe())), "\n")

//
//
fun print_trit(x: trit): void =

( case+ x of

| True() => println!("True")
| False() => println!("False")
| Maybe() => println!("Maybe")
)

//Compare the following with the provided truth tables.

val () = print_trit(trit_not(True))
val () = print_trit(trit_not(False))
val () = print_trit(trit_not(Maybe))

val () = print("\n")

val () = print_trit(trit_or(True, False))
val () = print_trit(trit_or(False, Maybe))
val () = print_trit(trit_or(Maybe, False))
val () = print_trit(trit_or(True, Maybe))
val () = print_trit(trit_or(False, False))
val () = print_trit(trit_or(Maybe, Maybe))
val () = print_trit(trit_or(True, True))
val () = print_trit(trit_or(False, True))
val () = print_trit(trit_or(Maybe, True))

val () = print("\n")

val () = print_trit(trit_and(True, False))
val () = print_trit(trit_and(False, Maybe))
val () = print_trit(trit_and(Maybe, False))
val () = print_trit(trit_and(True, Maybe))
val () = print_trit(trit_and(False, False))
val () = print_trit(trit_and(Maybe, Maybe))
val () = print_trit(trit_and(True, True))
val () = print_trit(trit_and(False, True))
val () = print_trit(trit_and(Maybe, True))