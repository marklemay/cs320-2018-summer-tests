#include "./../extra06.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

val() = assertloc(list0_forall2(SolveIt(100000), g0ofg1($list{int}(1, 9, 45, 55, 99, 297, 703, 999, 2223, 2728, 4879, 4950, 5050, 5292, 7272, 7777, 9999, 17344, 22222, 38962, 77778, 82656, 95121, 99999)), lam(x, y) => x=y))

val () = println!("All Kaprekar numbers less than 10,000: ", SolveIt(10000))
// Find these at http://oeis.org/A006886.