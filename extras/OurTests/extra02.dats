#include "./../extra02.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests
// remember to view tests critically.  If you see a mistake, please post a fix to the above repo.

fun loop1(start:int, stop:int, stream:stream($tup(int, int, int))):list0($tup(int, int, int)) =
  if start = stop
  then nil0()
  else (
    let
      val- stream_cons(s, st) = !stream;
    in
      list0_make_sing(s) + loop1(start+1, stop, st)
    end
  )

val() = assertloc(list0_forall(loop1(1, 320, SolveIt()), lam(x) => (x.0+x.1+x.2=12)&&(x.0 != x.1)&&(x.1 != x.2)&&(x.2 != x.0)&&(x.0=2||x.0=4||x.0=6)))  // Nothing should happen
