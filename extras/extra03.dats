(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
(*
Task
Flatten a generic list
*)
(* ****** ****** *)
//
datatype
glist(a:t@ype) =
  | glist_nil of ()
  | glist_cons of (gitem(a), glist(a))
and
gitem(a:t@ype) =
  | gitem_elt of (a) | gitem_list of glist(a)
//
extern
fun
{a:t@ype}
gflatten(xs: glist(a)): list0(a)
//
(* ****** ****** *)

(* end of [extra03.dats] *)
