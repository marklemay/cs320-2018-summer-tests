(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
(*
Task
Department Numbers

There is a highly organized city that has decided to assign a number
to each of their departments:

police department sanitation department fire department
                                          
Each department can have a number between 1 and 7   (inclusive).
                                          
The three department numbers are to be unique (different from each other) and must add up to the number 12.
                                          
The Chief of the Police doesn't like odd numbers and wants to have an even number for his department.
*)
(* ****** ****** *)
//
(*
Please implement a program in ATS to solve the above task
*)
extern
fun
SolveIt(): stream($tup(int, int, int))
//
(* ****** ****** *)

(* end of [extra02.dats] *)
