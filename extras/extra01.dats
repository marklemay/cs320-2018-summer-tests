(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
(*
Task
In what years between 2008 and 2121 will the 25th of
December be a Sunday? 
*)
(* ****** ****** *)
//
(*
Please implement a program in ATS to solve the above task
*)
extern
fun
SolveIt(): list0(int)
//
(* ****** ****** *)

(* end of [extra01.dats] *)
