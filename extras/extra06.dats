(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
(*
Please solve the following problem:
http://rosettacode.org/wiki/Kaprekar_numbers
*)
(* ****** ****** *)
//
// HX:
// Find all the Kapreka numbers less than n
//
extern
fun
SolveIt(n: int): list0(int)

(* ****** ****** *)
//
// Please give your implemenation of SolveIt as follows:
//
(* ****** ****** *)

(* end of [extra06.dats] *)
