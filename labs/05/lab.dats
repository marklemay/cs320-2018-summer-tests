#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//#include "share/atspre_staload_libats_ML.hats"
#include "share/HATS/atspre_staload_libats_ML.hats"

//we always need this to make c happy
implement main0() = ()

//lab doc availible at ???


datatype mystream_con(A:t@ype) =
  | mystream_nil  of ()
  | mystream_cons of (A, mystream(A))
where  mystream(A:t@ype) = lazy(mystream_con(A)) //just a mutual typedef

fun myprint(ls:mystream(int), i:int):void =
  if i <= 0
  then println!("...")
  else
    case+ !ls of
      | mystream_nil()         => ()
      | mystream_cons(x, rest) =>
        let
          val () = println!(x)
        in
          myprint(rest,i-1)
        end


//zeros forever...
val zeros =
  let
    fun zeroStream():mystream(int) =
      $delay( mystream_cons(0, zeroStream()) )
  in
    zeroStream()
  end

//val zeros = myprint(zeros , 10)

// nats = 0, 1, 2, 3, 4, ...
val nats =
  let
    fun natStream(n:int):mystream(int) =
      $delay( mystream_cons(n, natStream(n+1 )) )
  in
    natStream(0)
  end

val () = myprint(nats , 10)

// double_stream(nats) = 0, 0, 1, 1, 2, 2, ...
fun {X:t@ype} double_stream(xs:mystream(X)): mystream(X) =
  case+ !xs of
  | mystream_nil()        => $delay( mystream_nil())
  | mystream_cons(x, xs ) => $delay( mystream_cons(x, $delay( mystream_cons(x, double_stream(xs)) ) ))


val () = myprint(double_stream(nats)  , 10)

// nats = 0, 1, 2, 3, 4, ...
// n_stream(3, nats) = 0, 0, 0, 1, 1, 1, 2, 2, 2, ...
fun {X:t@ype} n_stream(n:int, xs:mystream(X)): mystream(X) =
  let
    fun preprend(n:int, x:X, xs: lazy( mystream(X))):mystream(X) =
      if n = 0
      then ! xs
      else $delay( mystream_cons(x,preprend(n-1,x, xs)) )
  in
    case+ !xs of
    | mystream_nil()        => $delay( mystream_nil())
    | mystream_cons(x, xs ) => preprend(n,x, $delay( n_stream(n,xs) )  )
  end


val () = myprint(n_stream(3,nats)  , 10)


// nats = 0, 1, 2, 3, 4, ...
// ones = 1, 1, 1, 1, 1, ...
// alternate_stream(nats, ones) =  0, 1, 1, 1, 2, 1, 3, 1, ...
fun {X:t@ype} alternate_stream(ls:mystream(X), rs:mystream(X)): mystream(X) =
  case+ (!ls, !rs)  of
  | (mystream_nil(), mystream_nil())                   => $delay( mystream_nil())
  | (mystream_nil(), xs)                               => $delay(xs)
  | (xs, mystream_nil())                               => $delay(xs)
  | (mystream_cons(xl, xsl ), mystream_cons(xr, xsr )) =>
      $delay(mystream_cons(xl, $delay(mystream_cons(xr, alternate_stream(xsl,xsr))) ))


val () = myprint(alternate_stream(zeros,nats)  , 10)

fun {X:t@ype} alternate_stream(ls:mystream(X), rs:mystream(X)): mystream(X) =
  case+ !ls  of
  | mystream_nil()          => rs
  | mystream_cons(xl, xsl ) =>
      $delay(mystream_cons(xl,  alternate_stream(rs,xsl)) )

val () = myprint(alternate_stream(zeros,nats)  , 10)

