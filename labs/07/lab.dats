#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//#include "share/atspre_staload_libats_ML.hats"
#include "share/HATS/atspre_staload_libats_ML.hats"

// to easily print streams
#include "./../../mylib/class.dats"

//we always need this to make c happy
implement main0() = ()

//lab doc availible at https://docs.google.com/document/d/1OzhOj5N_S86HHM-brxcySyZ72EZKdQhYKhp3b3zmzDI/edit?usp=sharing


// note: staircase_count(0) = 1

fun staircase_count(i:int):int =
  if i < 3
  then 1
  else staircase_count(i-1) + staircase_count(i-3)

val () = println!(staircase_count(13))


// tail recursive:
fun staircase_count(i:int):int =
  let
    fun aux(count:int, x:int, y:int, z:int):int =
      if count < 3
      then z
      else aux(count - 1, y, z, x+z)
  in
    aux(i,1,1,1)
  end


val () = println!(staircase_count(13))

//val () = println!(staircase_count(10))

//lazyness review



val q : lazy(lazy(lazy(int))) = $delay( $delay( $delay( 3 ) ) )
val () = println!( !(!(!(q))) )


// lazy( whatever type stuff )

val x: lazy(lazy(int)) = $delay((println!( "hard work") ;
                            $delay(( println!( "MORE hard work") ; 3
                                  ))
                               ))



val () = println!("lazy problem:")
val _ = !x // "hard work"
val () = println!("..")
val _ = !x // nothing
val () = println!("..")
val _ = !(!x) // "MORE hard work"
val () = println!("..")
val _ = !x // nothing


val three = 3

val threeagain = ( println!( "IGNORE ME") ; 3)


val () = println!(three = threeagain)

val () = println!(three = !(!x))

// stream review
//val staircase_counts: stream(int) =

//val () = println!(staircase_counts : stream(int))






#staload DFS = "./../../lectures/lecture-06-18/DFS.dats"

// interfach to play a game of "hangman"
extern fun isLetter(c:char, pos:int): bool


val allLetters:list0(char) =
  let
    fun aux(c:char):list0(char) =
      list0_cons(c, if c = 'z'
                    then list0_nil()
                    else aux(c+1))
  in
    aux('a')
  end




typedef node = $DFS.node  // lets us not have to write $DFS.node every time

assume $DFS.node = list0(char) // the type for our partial solution, in this case the prefix

implement {} $DFS.children(partialSolution:node) : list0(node) = //hw do we take a partial solution and find it's children?
  list0_map(list0_filter(allLetters, lam c => isLetter(c, length(partialSolution))),
  (lam c => list0_cons(c, partialSolution) ) )

val partialSolutions = $DFS.depth_first_search(list0_nil()) //the most partial solutions

















//  can see all the partial solutions
val () = println!(partialSolutions: list0(list0(char)))

// in our casse we want the last solution
fun {A:t@ype} last(ls:list0(A)): A =
    case- ls of
      | list0_cons(a, list0_nil()) => a
      | list0_cons(_, tail       ) => last(tail)

val () = println!()
val () = println!()


//and reversed
val () = println!(list0_reverse(last<list0(char)>(partialSolutions)))

//for a 10 char word even this toy example beats trying out every full solution
//26 * 10 <<<<<<<< 26 ^ 10








































































fun {A:t@ype} listGetOpt(ls:list0(A), pos:int): option0(A) =
  case+ ls of
    | list0_cons(a, tail) => if pos = 0
                             then Some0(a)
                             else listGetOpt(tail, pos -1)
    | list0_nil()         => None0()

implement isLetter(c:char, pos:int): bool =
  let
    val ls:list0(char) = string_explode("goodluckonthemidterm")
    val opt = listGetOpt<char>(ls, pos)
  in
    case+ opt of
      | Some0(x) => (x = c)
      | None0()  => false
  end