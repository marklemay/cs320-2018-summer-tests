#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//we always need this to make c happy
implement main0() = ()



datatype Bool =
 | true of ()
 | false of ()


datatype Dumb =
 | domb of ()


datatype Empty = |


datatype Crazy =
 | one of (int)
 | two of (string)
 | three of (string -> int)
 | four of ((string -> int) -> (string -> int))
 | five of (Crazy)




datatype Dessert =
  | Cake of (string) //could also be another type of flavor
  | Icecream of (Dessert)
  | Cookies of (int)  //could also be nat

val d = Cake("chocolate")
val c = Cookies(2)
val d2 = Icecream(Cake("chocolate"))

val d2 = Icecream(Icecream(Icecream(Cake("chocolate"))))



datatype Tree1 (a:t@ype)  =
  | Empty of ()
  | Join of (Tree1(a),a, Tree1(a))



fun size{a:t@ype}(tr:Tree1(a)): int =
  case+ tr of
//  | Empty         => 0 be careful not to do this! "Empty" will be interperted as a variable
  | Empty()         => 0
  | Join(tra,_,trb) => 1 + size(tra) + size(trb)



fun map{a,b:t@ype}(tr:Tree1(a), f : a -<cloref1> b): Tree1(b) =
  case+ tr of
  | Empty()         => Empty()
  | Join(tra,a,trb) => Join(map(tra, f),f(a), map(trb, f))



//fun fold{input,output:t@ype}(tr:Tree1(input), f : )





