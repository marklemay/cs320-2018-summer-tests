#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//#include "share/atspre_staload_libats_ML.hats"
#include "share/HATS/atspre_staload_libats_ML.hats"

//we always need this to make c happy
implement main0() = ()

//lab doc availible at ???


// nats = 0, 1, 2, 3, 4, ...
val nats: stream(int) =
  let
  fun natstream(n:int):stream(int) =
    $delay( stream_cons(n, natstream(n+1)) )
  in
    natstream(0)
  end


fun myprint(ls:stream(int), i:int):void =
  if i <= 0
  then println!("...")
  else
    case+ !ls of
      | stream_nil()         => ()
      | stream_cons(x, rest) =>
        let
          val () = println!(x)
        in
          myprint(rest,i-1)
        end


val () = myprint(nats, 10)

// nats = 0, 1, 2, 3, 4, ...
// split_stream(nats) = $tup(
//                           0, 2, 4, 6, ...
//                          ,
//                           1, 3, 5, 7, ...
//                          )
fun {X:t@ype} split_stream(xs:stream(X)): $tup(stream(X),stream(X)) =
  case+ !xs of
  | stream_nil()         => $tup($delay(stream_nil()),$delay(stream_nil()))
  | stream_cons(x, rest) => (
    case+ !rest of
    | stream_nil()              => $tup($delay(stream_cons(x, $delay(stream_nil()))), $delay(stream_nil()))
    | stream_cons(x2, restrest) => $tup($delay(stream_cons(x, (split_stream(restrest)).0)),
                                            $delay(stream_cons(x2, (split_stream(restrest)).1)))
    //doesn't work
//     let
//       val $tup(left,right) = split_stream(restrest)
//     in
//       $tup($delay(stream_cons(x, left )),
//                                        $delay(stream_cons(x2, right)))
//     end

    )

val $tup(left,right) = split_stream(nats)


val () = myprint(left, 10)
val () = myprint(right, 10)

//Write a datatype to represent different years of bu students.
//Freshmen should have nicknames represented by strings, seniors should have a bool which is true if they will graduate on time.
datatype students =
 | Freshmen of (string)
 | Sophomore of ()
 | Junior of ()
 | Senior of (bool)


fun graduate(s: students):int =
  case+ s of
  | Freshmen(_) => 66
  | Sophomore() => 65
  | Junior()    => 64
  | Senior(true) => 63
  | Senior(false) => 100

fun years(y:int): Option(students) =
  if y = 66 then Some(Freshmen("created"))
  else if y = 65 then Some(Sophomore())
  else if y = 64 then Some(Junior())
  else if y = 63 then Some(Senior(true))
  else if y = 100 then Some(Senior(false))
  else None()

fun years(y:int): Option(students) =
  case+ y of
  | 66  => Some(Freshmen("created"))
  | 65  => Some(Sophomore())
  | 64  => Some(Junior())
  | 63  => Some(Senior(true))
  | 100 => Some(Senior(false))
  | _   => None()

datatype Crazy =
 | one of (int)
 | two of (string)
 | three of (string -> int)
 | four of ((string -> int) -> (string -> int))
 | five of (Crazy)
 | one of (lazy(int))
 | two of (lazy(string))
 | three of (lazy(string -> int))
 | four of (lazy((string -> int) -> (string -> int)))
 | five of (lazy(Crazy))

val li: lazy(int) = $delay(7)
//val llllli: lazy(lazy(lazy(lazy(int)))) = $delay($delay($delay($delay($delay(7)))))
