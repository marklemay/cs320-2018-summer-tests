#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//#include "share/atspre_staload_libats_ML.hats"
#include "share/HATS/atspre_staload_libats_ML.hats"

//we always need this to make c happy
implement main0() = ()

//lab doc availible at https://docs.google.com/document/d/1SnmxAyM7cUb8Fem4ZJ4kCEirAQae3tY-FRC7gkVCPmU/edit?usp=sharing



datatype Tree(A:t@ype) =
 | empty of ()
 | node  of (Tree(A), A, Tree(A))


// we can create trees with the constructors
val easytree:Tree(int)  = node(node(empty(),4,empty()),3,node(node(empty(),7,empty()),6,empty()))

fun {A:t@ype}treeToList(tr:Tree(A)) : list0(A) =
  case+ tr of
  | empty()                         => list0_nil()
  | node(leftTree, data, rightTree) => treeToList(leftTree) + list0_cons( data, treeToList(rightTree))
// treeToList(leftTree)  data  treeToList(rightTree)
// hint: + is overloaded with append for list.  See: http://ats-lang.sourceforge.net/LIBRARY/libats/ML/SATS/DOCUGEN/HTML/list0.html



fun {A,B:t@ype}zipTree(tr:Tree(A), tr2:Tree(B)): Tree($tup(A,B)) =
  case- $tup(tr,tr2) of
  | $tup(empty()                  , _                        ) => empty()
  | $tup(_                        , empty()                  ) => empty()
  | $tup(node(lTreeA,dataA,rTreeA), node(lTreeB,dataB,rTreeB)) => node(zipTree(lTreeA,lTreeB), $tup(dataA, dataB), zipTree(rTreeA,rTreeB))

val ls = treeToList<$tup(int,int)>(zipTree<int,int>(easytree,easytree))
val () = println!(ls)  // $tup(4, 4), $tup(3, 3), $tup(7, 7), $tup(6, 6)

fun {A,B:t@ype}fold(tr:Tree(A), empty_case:B, node_case: (B,A,B) -<cloref1> B ):B =
  case+ tr of
  | empty()              => empty_case
  | node(left, a, right) => node_case(fold(left,empty_case,node_case), a, fold(right,empty_case,node_case))


//can find the height of the tree
val () = println!(fold<int,int>(easytree, 0, lam(l:int, i:int, r:int):int => if l > r then l + 1 else r + 1))  // 3
